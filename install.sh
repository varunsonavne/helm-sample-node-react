
chrts=$(cat names.txt | cut -d " " -f1)

for chart in $chrts;
do
	chartname=$(cat names.txt | grep -i $chart | cut -d " " -f2)
	helm list -A | grep -i $chart
	if [ $(echo $?) = 0 ]
	then
		helm upgrade -n development $chartname charts/$chart
	else
		helm install -n development $chartname charts/$chart
	fi
done
